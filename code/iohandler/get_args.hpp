#pragma once
//------------------------------------------------
//  GET_ARGS.HPP
//------------------------------------------------

#ifndef GET_ARGS_HPP
#define GET_ARGS_HPP

#include "defs.hpp"

int read_parameters(int argc, char* argv[], brain_params& brain_parameters);

#endif
