//------------------------------------------------
//  PARSE_STRUCTURE.CPP
//------------------------------------------------

#include "parse_structure.hpp"

// Libraries

#include <vector>
#include <algorithm>

// CODE

structure_param parse_one_part_tokenlist( std::vector<std::string> token_list);
structure_param parse_two_part_tokenlist( std::vector<std::string> token_list);

structure_param parse_structure(std::string structure_string)
{
	std::vector<std::string> layer_tokens;

	size_t token_start = structure_string.find("{");
	size_t token_end   = structure_string.find("}");
	while ( token_start != std::string::npos && token_end != std::string::npos )
	{
		layer_tokens.push_back( structure_string.substr(token_start + 1, token_end - token_start - 1) );
		structure_string.erase(token_start, token_end - token_start + 1);

		token_start = structure_string.find("{");
		token_end   = structure_string.find("}");
	}

	// Check if the syntax of the tokens is formally correct
	bool one_part_tokens = false, two_part_tokens = false;
	for (auto& layer : layer_tokens)
	{
		if (layer.find(",") == std::string::npos)
		{
			one_part_tokens = true;
		}
		else
		{
			two_part_tokens = true;
		}
	}

	// Report every error we can have so far
	if ( one_part_tokens == two_part_tokens || structure_string.size() != 0 )
	{
		throw(std::string("Somehow you messed up the structure argument. \
					Check if every arguments is in curly braces."));
	}

	// Start to extract the parameters
	structure_param structure_parameter;
	if (one_part_tokens)
	{
		try
		{
			structure_parameter = parse_one_part_tokenlist( layer_tokens);
		}
		catch(std::string s)
		{
			throw s;
		}
	}
	else
	{
		try
		{
			structure_parameter = parse_two_part_tokenlist( layer_tokens);
		}
		catch(std::string s)
		{
			throw s;
		}
	}
	return structure_parameter;
}

structure_param parse_one_part_tokenlist( std::vector<std::string> token_list)
{
	std::vector<unsigned int> neurons_per_layer;

	for (auto& token : token_list)
	{
		if ( token.find_first_not_of("0123456789") != std::string::npos )
		{
			throw(std::string("In the structure argument, you tried to pass something which is not a number.\n"));
		}
		else
		{
			neurons_per_layer.push_back(std::stod(token) );
		}
	}

	return structure_param( neurons_per_layer,
			std::vector<activation_function> (neurons_per_layer.size(), activation_function::LINEAR) );
}
					
structure_param parse_two_part_tokenlist( std::vector<std::string> token_list)
{
	std::vector<unsigned int> neurons_per_layer;
	std::vector<activation_function> function_per_layer;

	std::string number, function;
	size_t token_separator_pos;

	for (auto& token : token_list)
	{
		token_separator_pos = token.find_first_of(",;");
		number = token.substr(0,token_separator_pos - 1);
		function = token.substr(token_separator_pos + 1, std::string::npos);

		if ( number.find_first_not_of("0123456789") != std::string::npos )
		{
			throw(std::string("In the structure argument, you tried to pass something which is not a number.\n"));
		}
		else
		{
			neurons_per_layer.push_back(std::stod(token) );
		}

		std::transform(function.begin(), function.end(), function.begin(), ::tolower);
		if ( function == "linear")
		{
			function_per_layer.push_back(activation_function::LINEAR);
		}
		else if ( function == "fermi")
		{
			function_per_layer.push_back(activation_function::FERMI);
		}
		else if ( function == "tanh" )
		{
			function_per_layer.push_back(activation_function::TANH);
		}
		else
		{
			throw(std::string("Activation function " + function + " is not known.\n") );
		}
	}

	return structure_param( neurons_per_layer, function_per_layer);	
}
					

