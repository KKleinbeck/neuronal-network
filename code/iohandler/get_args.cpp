//------------------------------------------------
//  GET_ARGS.CPP
//------------------------------------------------

#include "get_args.hpp"

// Libraries

#include <iostream>
#include <fstream>
#include <string>
#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include "parse_structure.hpp"

// CODE

void conflicting_options(const po::variables_map& vm, const char* opt1, const char* opt2)
{
	if(vm.count(opt1) && !vm[opt1].defaulted() && vm.count(opt2) && !vm[opt2].defaulted() )
	{
		throw std::string("Conflicting options.");
	}
}

int read_parameters(int argc, char* argv[], brain_params& brain_parameters)
{
	std::string config_path, structure, default_config_path("./neuralnet.cfg");
	try
	{
		po::options_description generic_options("Allowed options");
		generic_options.add_options()
			("help,h",			"produce help message")
			("version",			"display informations about the version")
            ("config,c", po::value<std::string>(&config_path)->default_value(default_config_path),
            	"path to a config-file");

		po::options_description general_options("Options valid both from command line and config file");
		general_options.add_options()
			("nobias,n",		"disables a bias neuron")
			("feed_forward",	"use a 'feed forward' network (default)")
			("full_forward",	"use a fully forward connected network")
			("online",			"let the network learn online (default)")
			("offline",			"let the network learn offline")
			("backprop",		"learn via back propagation of error (default)")
			("rprop",			"learn via rprop (not yet implemented!)");

		po::options_description config_only("Options valid only in the config file");
		config_only.add_options()
			("structure", po::value<std::string>(&structure)->default_value("{41,tanh}{141,tanh}{41,linear}"),
			 	"description of each layer in the network");

		po::options_description cmd_line_options("Options valid from the command line");
		cmd_line_options.add(general_options).add(generic_options);

		po::options_description config_options("Options valid in the config file");
		config_options.add(general_options).add(config_only);

		po::options_description all_options("Full list of all options");
		all_options.add(generic_options).add(general_options).add(config_only);

		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, cmd_line_options), vm);
		po::notify(vm);
        
		std::ifstream config_file(config_path.c_str());
        if (!config_file)
        {
			std::cout << "Can not open config file: " << config_path << "\n";
			if (config_path == default_config_path)
			{
				std::cout << "It seems that your default config is missing."
					<< "I will create an empty version for you.\n";
				std::ofstream oconfig_file(default_config_path.c_str());
				oconfig_file << "#\n"
							<< "# Automatic generated config\n"
							<< "#\n\n"
							<< "# Remove comment on next line to set the network structure\n"
							<< "# structure = {41,tanh}{141,tanh}{41,linear}";
				oconfig_file.close();
			}
            return -1;
        }
        else
        {
            store(parse_config_file(config_file, config_options), vm);
            notify(vm);
        }
		config_file.close();

		conflicting_options(vm, "feed_forward", "full_forward");
		conflicting_options(vm, "online", "offline");
		conflicting_options(vm, "backprop", "rprop");

		if(vm.count("help") )
		{
			std::cout << all_options << std::endl;
			return -1;
		}
		if(vm.count("version") )
		{
			std::cout << "Beta 0.4 --- Working title quantsoft" << std::endl;
			return -1;
		}
		if(vm.count("nobias") )
		{
			brain_parameters.has_bias_neuron = false;
		}
		if(vm.count("feed_forward") )
		{
			brain_parameters.connection = connection_param::FEED_FORWARD;
		}
		if(vm.count("full_forward") )
		{
			brain_parameters.connection = connection_param::FULL_FORWARD;
		}
		if(vm.count("online") )
		{
			brain_parameters.timing = timing_param::ONLINE;
		}
		if(vm.count("offline") )
		{
			brain_parameters.timing = timing_param::OFFLINE;
		}
		if(vm.count("backprop") )
		{
			brain_parameters.method = method_param::BACKPROP;
		}
		if(vm.count("rprop") )
		{
			brain_parameters.method = method_param::RPROP;
		}

		brain_parameters.structure = parse_structure(structure);
	}
	//catch(po::exception& e)
	//{
		//std::std::cerr << "ERROR: " << e.what() << std::endl;
		//return 1;
	//}
	catch(...)
	{
		std::cerr << "Unknown error!" << std::endl;
		return 1;
	}
	return 0;
}
