#pragma once
//------------------------------------------------
//  PARSE_STRUCTURE.HPP
//------------------------------------------------

#ifndef PARSE_STRUCTURE_HPP
#define PARSE_STRUCTURE_HPP

#include <string>
#include "defs.hpp"

structure_param parse_structure(std::string structure_string);

#endif
