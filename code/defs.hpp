#pragma once
//------------------------------------------------
//  DEFS.HPP
//------------------------------------------------

#ifndef DEFS_HPP
#define DEFS_HPP

#include <vector>
#include <array>
#include "neuron.hpp"

// name shortcuts
typedef std::vector<std::array<std::vector<double>, 2>> testcases;
typedef std::vector<std::vector<neuron>> neuron_matrix;

// Network defining structure
enum class connection_param { FEED_FORWARD, FULL_FORWARD };
enum class timing_param		{ OFFLINE, ONLINE };
enum class method_param 	{ BACKPROP, RPROP };

enum class activation_function
							{ TANH, FERMI, RECTIFIER, LINEAR };

struct structure_param
{
	std::vector<unsigned int> neurons_per_layer;
	std::vector<activation_function> act_func_per_layer;

	structure_param() :
		neurons_per_layer(1,1), act_func_per_layer(1, activation_function::LINEAR) {};

	structure_param(std::vector<unsigned int> npl_in,
			std::vector<activation_function> afpl_in) :
		neurons_per_layer(npl_in), act_func_per_layer(afpl_in) {};
};

struct brain_params
{
	connection_param connection;
	timing_param timing;
	method_param method;
	structure_param structure;

	bool has_bias_neuron;

	brain_params(connection_param c_in = connection_param::FEED_FORWARD,
			timing_param t_in = timing_param::ONLINE,
			method_param m_in = method_param::BACKPROP,
			structure_param s_in = structure_param(std::vector<unsigned int> (1,1),
				std::vector<activation_function> (1, activation_function::LINEAR) ),
			bool b_in = true) :
		connection(c_in), timing(t_in), method(m_in), structure(s_in), has_bias_neuron(b_in) {}
};


#endif
