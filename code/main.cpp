#include <iostream>
#include <chrono>

#include "defs.hpp"

#include "network.hpp"
#include "diff_funcs.hpp"
#include "rne.hpp"
#include "get_args.hpp"
#include "parse_structure.hpp"

using namespace std;

vector<double> range(double xmin, double xmax, int steps = 101)
{
	vector<double> x(steps);
	double step = (xmax-xmin) / (steps-1);

	for (int i = 0; i < steps; ++i)
	{
		x[i] = xmin + step*i;
	}
	return x;
}

int main(int argc, char* argv[])
{
	brain_params brain_parameters;
	switch(read_parameters(argc, argv, brain_parameters) )
	{
		case -1: // help message invoked
			return 0;
		case 0:
			break;
		default:
			return 1;
	}

	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
	rne random_engine(seed);

	network net(brain_parameters, random_engine);

	vector<testfunction*> funcs;
	funcs.push_back(new exp_func(random_engine) );
	funcs.push_back(new square_func(random_engine) );
	funcs.push_back(new tanh_func(random_engine) );

	testcases t(funcs.size() );
	for(auto& test: t)
	{
		test.front().resize(brain_parameters.structure.neurons_per_layer.front() );
		test.back().resize(brain_parameters.structure.neurons_per_layer.back() );
	}
	vector<double> x = range(-2,2,brain_parameters.structure.neurons_per_layer.front() );


	//net.monitornet();
	double error;
	//do
	//{
	// Generate testcases:

		for (unsigned int j = 0; j < funcs.size(); ++j)
		{
			t[j] = funcs[j]->generate_testcase(x);
		}
	for (int i = 0; i < 30; ++i)
	{
		error = net.train_epoch(t);
		cout << "Fehler: " << error << endl;
	}
	//} while (error > 0.2);
	//net.monitornet();

	testfunction* final_verification = new fermi_func(random_engine);
	array<vector<double>, 2> final_test = final_verification->generate_testcase(x);
	vector<double> test_result = net.solve(final_test.front() );
	double verification_error = 0;
	for (unsigned i = 0; i < test_result.size(); ++i)
	{
		verification_error += fabs(test_result[i] - final_test.back()[i] );
	}
	cout << "\nTest: " << verification_error << endl;
	return 0;
}
