//------------------------------------------------
//  RNE.CPP
//------------------------------------------------

#include "rne.hpp"

// Libraries

// Code

using namespace std;

double rne::range(double min, double max)
{
	uniform_real_distribution<double> real_dist(min, max);
	return real_dist(_rng);
}

double rne::splitrange(double absmin, double absmax)
{
	uniform_real_distribution<double> real_dist(absmin, absmax);
	if ( (real_dist(_rng) - (absmax + absmin)/2.) > 0)
	{
		return real_dist(_rng);
	}
	return -real_dist(_rng);
}

int rne::shuffle_index(int i)
{
		uniform_int_distribution<int> int_dist(0, i);
		return int_dist(_rng);
}
