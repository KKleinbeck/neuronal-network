#pragma once
//------------------------------------------------
//  RNE.HPP - RANDOM NUMBER ENGINE
//------------------------------------------------

#ifndef RNE_HPP
#define RNE_HPP


// Libraries
#include <random>
#include <vector>

// My Libraries

// Typedefs and Structures



// Rne : Random number engine, provides several distributions

class rne
{
	private:
		std::default_random_engine _rng;
	
	public:
		rne(double seed) : _rng(seed) {};

		double range(double min, double max);
		double splitrange(double absmin, double absmax);
		int shuffle_index(int i);

		std::default_random_engine get_engine() const {return _rng;}
};

// Shuffle: shuffle the contents of an vector

template<typename T>
void shuffle(T& input_vector, rne const& random_engine);

#endif
