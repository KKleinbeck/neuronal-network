#pragma once
//------------------------------------------------
//  TESTFUNC.HPP
//------------------------------------------------

#ifndef TESTFUNC_HPP
#define TESTFUNC_HPP


// Libraries
#include <vector>
#include <array>

// My Libraries
#include "../rne/rne.hpp"

// Typedefs and Structures



// Testfunc: parent class to create proper test

class testfunction
{
	protected:
		rne _random_engine;

		virtual void _randomize_parameters(void) =0;
		virtual std::vector<double> _solution(std::vector<double> x) =0;
	
	public:
		testfunction(rne &random_engine) : _random_engine(random_engine) {};
		virtual std::array<std::vector<double>, 2> generate_testcase(std::vector<double> x) =0;
};

#endif
