#pragma once
//------------------------------------------------
//  DIFF_FUNC.HPP
//------------------------------------------------

#ifndef DIFF_FUNC_HPP
#define DIFF_FUNC_HPP


// Libraries
#include <vector>
#include <array>

// My Libraries
#include "testfunc.hpp"

// Typedefs and Structures



// Harmonic: Harmonic potential

class exp_func: public testfunction
{
	private:
		double _k;
		double _amp;
	
	protected:
		virtual void _randomize_parameters(void);
		virtual std::vector<double> _function(std::vector<double> x);
		virtual std::vector<double> _solution(std::vector<double> x);


	public:
		exp_func(rne &random_engine): testfunction(random_engine), _k(1), _amp(1) {};

		virtual std::array<std::vector<double>, 2> generate_testcase(std::vector<double> x);
};

class square_func: public testfunction
{
	private:
		double _amp;
		double _offset;
	
	protected:
		virtual void _randomize_parameters(void);
		virtual std::vector<double> _function(std::vector<double> x);
		virtual std::vector<double> _solution(std::vector<double> x);


	public:
		square_func(rne &random_engine): testfunction(random_engine), _amp(0.5), _offset(0.5) {};

		virtual std::array<std::vector<double>, 2> generate_testcase(std::vector<double> x);
};

class tanh_func: public testfunction
{
	private:
		double _amp;
		double _offset;
	
	protected:
		virtual void _randomize_parameters(void);
		virtual std::vector<double> _function(std::vector<double> x);
		virtual std::vector<double> _solution(std::vector<double> x);


	public:
		tanh_func(rne &random_engine): testfunction(random_engine), _amp(1), _offset(0.5) {};

		virtual std::array<std::vector<double>, 2> generate_testcase(std::vector<double> x);
};

class fermi_func: public testfunction
{
	private:
		double _amp;
		double _temp;
	
	protected:
		virtual void _randomize_parameters(void);
		virtual std::vector<double> _function(std::vector<double> x);
		virtual std::vector<double> _solution(std::vector<double> x);


	public:
		fermi_func(rne &random_engine): testfunction(random_engine), _amp(1), _temp(1) {};

		virtual std::array<std::vector<double>, 2> generate_testcase(std::vector<double> x);
};

#endif
