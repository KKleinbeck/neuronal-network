#pragma once
//------------------------------------------------
//  HARMONIC.HPP
//------------------------------------------------

#ifndef HARMONIC_HPP
#define HARMONIC_HPP


// Libraries
#include <vector>
#include <array>

// My Libraries
#include "testfunc.hpp"

// Typedefs and Structures



// Harmonic: Harmonic potential

class harmonic: testfunction
{
	private:
		double _offset;
		double _omega;
	
	protected:
		virtual void _randomize_parameters(void);
		virtual std::vector<double> _potential(std::vector<double> x);
		virtual std::vector<double> _solution(std::vector<double> x);


	public:
		harmonic(rne &random_engine): testfunction(random_engine), _offset(0), _omega(1) {};

		virtual std::array<std::vector<double>, 2> generate_testcase(std::vector<double> x);
};

#endif
