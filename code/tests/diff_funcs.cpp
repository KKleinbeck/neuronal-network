//------------------------------------------------
//  HARMONIC.CPP
//------------------------------------------------

#include "diff_funcs.hpp"

// Libraries
#include <cmath>

// Code

using namespace std;

// --- exp_func ---

void exp_func::_randomize_parameters()
{
	_k   = _random_engine.range(0.1, 1.0);
	_amp = _random_engine.splitrange(0.1, 1.0);
}

vector<double> exp_func::_function(vector<double> x)
{
	int _size = x.size();
	vector<double> func(_size );

	for (int i = 0; i < _size; ++i)
	{
		func[i] = _amp * exp(_k*x[i] );
	}

	return func;
}

vector<double> exp_func::_solution(vector<double> x)
{
	int _size = x.size();
	vector<double> solution(_size );

	for (int i = 0; i < _size; ++i)
	{
		solution[i] = _amp * _k * exp(_k*x[i] );
	}

	return solution;
}

array<vector<double>, 2> exp_func::generate_testcase(vector<double> x)
{
	array<vector<double>, 2> test_case{{vector<double>(x.size()),
										vector<double>(x.size())}};

	_randomize_parameters();
	test_case[0] = _function(x);
	test_case[1] = _solution(x);

	return test_case;
}

// --- square_func ---

void square_func::_randomize_parameters()
{
	_amp    = _random_engine.range(0.1, 0.5);
	_offset = _random_engine.range(-0.5, 0.5);
}

vector<double> square_func::_function(vector<double> x)
{
	int _size = x.size();
	vector<double> func(_size );

	for (int i = 0; i < _size; ++i)
	{
		func[i] = _amp * (x[i] - _offset) * (x[i] - _offset);
	}

	return func;
}

vector<double> square_func::_solution(vector<double> x)
{
	int _size = x.size();
	vector<double> solution(_size );

	for (int i = 0; i < _size; ++i)
	{
		solution[i] = 2 * _amp * (x[i] - _offset);
	}

	return solution;
}

array<vector<double>, 2> square_func::generate_testcase(vector<double> x)
{
	array<vector<double>, 2> test_case{{vector<double>(x.size()),
										vector<double>(x.size())}};

	_randomize_parameters();
	test_case[0] = _function(x);
	test_case[1] = _solution(x);

	return test_case;
}

// --- tanh_func ---

void tanh_func::_randomize_parameters()
{
	_amp    = _random_engine.range(0.1, 1);
	_offset = _random_engine.range(-0.5, 0.5);
}

vector<double> tanh_func::_function(vector<double> x)
{
	int _size = x.size();
	vector<double> func(_size );

	for (int i = 0; i < _size; ++i)
	{
		func[i] = _amp * tanh(x[i]-_offset);
	}

	return func;
}

vector<double> tanh_func::_solution(vector<double> x)
{
	int _size = x.size();
	vector<double> solution(_size );

	for (int i = 0; i < _size; ++i)
	{
		solution[i] = _amp * (1 - tanh(x[i]-_offset) * tanh(x[i]-_offset) );
	}

	return solution;
}

array<vector<double>, 2> tanh_func::generate_testcase(vector<double> x)
{
	array<vector<double>, 2> test_case{{vector<double>(x.size()),
										vector<double>(x.size())}};

	_randomize_parameters();
	test_case[0] = _function(x);
	test_case[1] = _solution(x);

	return test_case;
}

// --- fermi_func ---

void fermi_func::_randomize_parameters()
{
	_amp  = _random_engine.range(0.1, 1);
	_temp = _random_engine.range(1, 2);
}

vector<double> fermi_func::_function(vector<double> x)
{
	int _size = x.size();
	vector<double> func(_size );

	for (int i = 0; i < _size; ++i)
	{
		func[i] = _amp / (1 - exp(-x[i]/_temp) );
	}

	return func;
}

vector<double> fermi_func::_solution(vector<double> x)
{
	int _size = x.size();
	vector<double> solution(_size );

	for (int i = 0; i < _size; ++i)
	{
		solution[i] = (_amp / _temp) * exp(-x[i]/_temp) / ((1-exp(-x[i]/_temp)) * (1-exp(-x[i]/_temp)) );
	}

	return solution;
}

array<vector<double>, 2> fermi_func::generate_testcase(vector<double> x)
{
	array<vector<double>, 2> test_case{{vector<double>(x.size()),
										vector<double>(x.size())}};

	_randomize_parameters();
	test_case[0] = _function(x);
	test_case[1] = _solution(x);

	return test_case;
}

