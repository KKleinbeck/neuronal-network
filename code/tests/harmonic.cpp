//------------------------------------------------
//  HARMONIC.CPP
//------------------------------------------------

#include "harmonic.hpp"

// Libraries
#include <cmath>

// Code

using namespace std;

void harmonic::_randomize_parameters()
{
	_omega = _random_engine.range(1.0, 3.0);
	_offset= _random_engine.range(-1.0, 1.0);
}

vector<double> harmonic::_potential(vector<double> x)
{
	vector<double> potential(x.size() );
	int size = x.size();

	for (int i = 0; i < size; ++i)
	{
		potential[i] = _omega*_omega*(x[i] - _offset)*(x[i] - _offset);
	}

	return potential;
}

vector<double> harmonic::_solution(vector<double> x)
{
	vector<double> solution(x.size() );
	int size = x.size();
	double N = 1/sqrt(2) * pow(_omega/M_PI, 0.25);

	for (int i = 0; i < size; ++i)
	{
		solution[i] = N * exp(-_omega*x[i]*x[i]/2.);
	}

	return solution;
}

array<vector<double>, 2> harmonic::generate_testcase(vector<double> x)
{
	array<vector<double>, 2> test_case{{vector<double>(x.size()),
										vector<double>(x.size())}};

	_randomize_parameters();
	test_case[0] = _potential(x);
	test_case[1] = _solution(x);

	return test_case;
}


