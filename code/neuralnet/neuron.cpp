//------------------------------------------------
//  NEURON.CPP
//------------------------------------------------

#include "neuron.hpp"
#include <iostream>

// Libraries
#include "config.hpp"

// CODE

neuron& neuron::operator= (const neuron& rhs)
{
	if(this != &rhs)
	{
		for (auto& rhso: rhs._origins)
		{
			_origins.push_back(origin(rhso.linked_neuron, rhso.w) );
		}
		for (auto& rhst: rhs._targets)
		{
			_targets.push_back(target(rhst.linked_neuron, rhst.w) );
		}

		_act = rhs._act;
		_input = rhs._input;
		_delta = rhs._delta;

		_activator = rhs._activator;

		out = rhs.out;
	}
	return *this;
}

void neuron::activate()
{
	_input = 0;
	for (auto const& origin: _origins)
	{
		_input += origin.linked_neuron.out * origin.w;
	}
	_act = _activator->func(_input);
	out = _act;
}

void neuron::calculate_delta()
{
	_delta = 0;
	for (auto const& target: _targets)
	{
		_delta += target.linked_neuron._delta * target.w;
	}
	_delta *= _activator->dfunc(_input);
}

void neuron::calculate_delta(double diff_to_example)
{
	_delta = _activator->dfunc(_input) * diff_to_example;
}

void neuron::train_connection(double learning_rate)
{
	for (auto& origin: _origins)
	{
		origin.w += learning_rate * origin.linked_neuron.out * _delta;
	}
}

void link_neurons(neuron& predecessor, neuron& successor, double rate)
{
	successor._origins.push_back(origin(predecessor, rate) );
	predecessor._targets.push_back(target(successor, successor._origins.back().w) );
}

void neuron::monitor()
{
	for (auto const& pre: _origins)
	{
		std::cout << pre.w << "  ";
	}
}

void neuron::monitor_delta()
{
	std::cout << _delta;
}
