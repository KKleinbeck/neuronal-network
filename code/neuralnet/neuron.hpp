#pragma once
//------------------------------------------------
//  NEURON.HPP
//------------------------------------------------

#ifndef NEURON_HPP
#define NEURON_HPP


// Libraries
#include <vector>
#include <memory>

// My Libraries
#include "fact.hpp"

// Typedefs and Structures

class neuron;

struct origin
{
	neuron& linked_neuron;
	double w;

	origin(neuron& ln, double win) : linked_neuron(ln), w(win) {};
};

struct target
{
	neuron& linked_neuron;
	double& w;

	target(neuron& ln, double& win) : linked_neuron(ln), w(win) {};
};


// Neuron : A single neutron in the network

class neuron
{
	private:
		double _act;
		double _input;
		double _delta;

		std::shared_ptr<essential::activator> _activator;
		
		std::vector<origin> _origins;
		std::vector<target> _targets;

	public:
		double out;

		neuron(std::shared_ptr<essential::activator> activator
				= std::unique_ptr<essential::activator> (new essential::act_linear) )
			: _act(0), _input(0), _delta(0), _activator(activator), out(0) {};
		neuron& operator= (const neuron& rhs);

		void activate(void);

		void calculate_delta(void);
		void calculate_delta(double diff_to_example);

		void train_connection(double learning_rate);

		friend void link_neurons(neuron& predecessor, neuron& successor, double rate);

		void monitor(void);
		void monitor_delta(void);
};

#endif
