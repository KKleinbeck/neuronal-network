#pragma once
//------------------------------------------------
//  ERRFCT.HPP
//------------------------------------------------

#ifndef ERRFCT_HPP
#define ERRFCT_HPP

#include <vector>

double _errfct(std::vector<double> errors);

#endif
