//------------------------------------------------
//  ERRFCT.CPP
//------------------------------------------------

#include "errfct.hpp"

// Libraries
#include "config.hpp"
#include <cmath>
#include <algorithm>
#include <iostream>

// CODE

using namespace std;

#ifdef ERRFCT_ABS

double _errfct(std::vector<double> errors)
{
	double error = 0;
	for_each(errors.begin(), errors.end(), [&error](double err_i){error += fabs(err_i);} );
	return error;
}

#endif //ERRFCT_ABS



#ifdef ERRFCT_SQUARE

double _errfct(vector<double> errors)
{
	double error = 0;
	for_each(errors.begin(), errors.end(), [&error](double err_i){error += err_i*err_i;} );
	return error;
}

#endif //ERRFCT_SQUARE



#ifdef ERRFCT_EUKLID

double _errfct(vector<double> errors)
{
	double error = 0;
	for_each(errors.begin(), errors.end(), [&error](double err_i){error += err_i*err_i;} );
	return sqrt(error);
}

#endif //ERRFCT_EUKLID
