//------------------------------------------------
//  FACT.CPP
//------------------------------------------------

#include "fact.hpp"

// Libraries
#include <cmath>

// CODE

namespace essential
{
	 double act_linear::func(double in) {return in;}
	 double act_linear::dfunc(double in) {return 1;}

	 double act_tanh::func(double in) {return tanh(in);}
	 double act_tanh::dfunc(double in) {return 1 - tanh(in)*tanh(in);}

	 double act_fermi::func(double in) {return 1./(1-exp(-in) );}
	 double act_fermi::dfunc(double in) {return  exp(-in)/( (1-exp(-in))*(1-exp(-in)) );}

	 double act_rectifier::func(double in) {return ((in > 0) ? in : 0);}
	 double act_rectifier::dfunc(double in) {return ((in > 0) ? 1 : 0);}
} //namespace essential
