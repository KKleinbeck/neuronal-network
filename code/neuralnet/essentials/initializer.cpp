//------------------------------------------------
//  INITIALIZER.CPP
//------------------------------------------------

#include "initializer.hpp"

// Libraries
#include <vector>

// CODE

namespace essential
{
	void initialize_feed_forward(neuron_matrix& neurons, rne& random_engine)
	{
		double rate;

		for (std::vector<std::vector<neuron>>::iterator layer = neurons.begin() + 1;
				layer != neurons.end(); ++layer)
		{
			std::vector<std::vector<neuron>>::iterator prev_layer = layer - 1;
			for (auto& successors: *layer)
			{
				for (auto& predecessors: *prev_layer)
				{
					rate = random_engine.splitrange(0.05,0.5);
					link_neurons(predecessors, successors, rate);
				}
			}
		}
	}
	

	void initialize_feed_forward_w_bias(neuron_matrix& neurons, rne& random_engine, neuron& bias)
	{
		double rate;

		for (std::vector<std::vector<neuron>>::iterator layer = neurons.begin() + 1;
				layer != neurons.end(); ++layer)
		{
			std::vector<std::vector<neuron>>::iterator prev_layer = layer - 1;
			for (auto& successors: *layer)
			{
				for (auto& predecessors: *prev_layer)
				{
					rate = random_engine.splitrange(0.05,0.5);
					link_neurons(predecessors, successors, rate);
				}
				rate = random_engine.splitrange(0.05,0.5);
				link_neurons(bias, successors, rate);
			}
		}
	}
	

	void initialize_full_forward(neuron_matrix& neurons, rne& random_engine)
	{
		double rate;

		for (std::vector<std::vector<neuron>>::iterator layer = neurons.begin() + 1;
				layer != neurons.end(); ++layer)
		{
			for (std::vector<std::vector<neuron>>::iterator prev_layers = neurons.begin();
					prev_layers != layer; ++prev_layers)
			{
				for (auto& successors: *layer)
				{
					for (auto& predecessors: *prev_layers)
					{
						rate  = random_engine.splitrange(0.05,0.5);
						link_neurons(predecessors, successors, rate);
					}
				}
			}
		}
	}


	void initialize_full_forward_w_bias(neuron_matrix& neurons, rne& random_engine, neuron& bias)
	{
		double rate;

		for (std::vector<std::vector<neuron>>::iterator layer = neurons.begin() + 1;
				layer != neurons.end(); ++layer)
		{
			for (std::vector<std::vector<neuron>>::iterator prev_layers = neurons.begin();
					prev_layers != layer; ++prev_layers)
			{
				for (auto& successors: *layer)
				{
					for (auto& predecessors: *prev_layers)
					{
						rate  = random_engine.splitrange(0.05,0.5);
						link_neurons(predecessors, successors, rate);
					}
					rate = random_engine.splitrange(0.05,0.5);
					link_neurons(bias, successors, rate);
				}
			}
		}
	}
} //namespace essential
