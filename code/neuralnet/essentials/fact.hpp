#pragma once
//------------------------------------------------
//  FACT.HPP
//------------------------------------------------

#ifndef FACT_HPP
#define FACT_HPP

namespace essential
{
	struct activator
	{
		virtual double func(double) = 0;
		virtual double dfunc(double) = 0;
	};

	struct act_linear : activator
	{
		virtual double func(double);
		virtual double dfunc(double);
	};
	
	struct act_tanh : activator
	{
		virtual double func(double);
		virtual double dfunc(double);
	};

	struct act_fermi : activator
	{
		virtual double func(double);
		virtual double dfunc(double);
	};

	struct act_rectifier : activator
	{
		virtual double func(double);
		virtual double dfunc(double);
	};
} //namespace essentials

#endif
