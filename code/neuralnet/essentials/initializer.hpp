#pragma once
//------------------------------------------------
//  INITIALIZER.HPP
//------------------------------------------------

#ifndef INITIALIZER_HPP
#define INITIALIZER_HPP

// Libraries

// My Libraries
#include "neuron.hpp"
#include "rne.hpp"
#include "defs.hpp"

// Typedefs and Structures

// Initializer Functions

namespace essential
{
	void initialize_feed_forward		(neuron_matrix& neurons, rne& random_engine);
	void initialize_feed_forward_w_bias	(neuron_matrix& neurons, rne& random_engine, neuron& bias);

	void initialize_full_forward		(neuron_matrix& neurons, rne& random_engine);
	void initialize_full_forward_w_bias	(neuron_matrix& neurons, rne& random_engine, neuron& bias);
} //essential 

#endif
