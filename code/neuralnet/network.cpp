//------------------------------------------------
//  NETWORK.CPP
//------------------------------------------------

#include "network.hpp"

// Libraries
#include <iostream>
#include <algorithm> //std::shuffle
#include <map>

#include "config.hpp"
#include "errfct.hpp"
#include "initializer.hpp"
#include "fact.hpp"

// CODE

// Constructors

network::network(brain_params brain_parameters, rne &random_engine) :
	_brain_parameters(brain_parameters),
	_neurons(_brain_parameters.structure.neurons_per_layer.size() ),
	_random_engine(random_engine)
{
	// Throw error if neurons_per_layer has size 0 or 1
	
	std::map<activation_function, std::shared_ptr<essential::activator> > activator_ptrs;
	activator_ptrs[activation_function::TANH]		= std::unique_ptr<essential::activator>
														(new essential::act_tanh);
	activator_ptrs[activation_function::FERMI]		= std::unique_ptr<essential::activator>
														(new essential::act_fermi);
	activator_ptrs[activation_function::RECTIFIER]	= std::unique_ptr<essential::activator>
														(new essential::act_rectifier);
	activator_ptrs[activation_function::LINEAR]		= std::unique_ptr<essential::activator>
														(new essential::act_linear);

	for (unsigned i = 0; i < _brain_parameters.structure.neurons_per_layer.size(); ++i)
	{
		_neurons[i].resize(_brain_parameters.structure.neurons_per_layer[i],
				activator_ptrs[_brain_parameters.structure.act_func_per_layer[i] ]);
	}
	_initialize_random_rates();
}


// Methods

void network::_initialize_random_rates()
{
	switch (_brain_parameters.connection)
	{
		case connection_param::FEED_FORWARD:
		{
			if(_brain_parameters.has_bias_neuron)
			{
				essential::initialize_feed_forward_w_bias(_neurons, _random_engine, _bias);
			}
			else
			{
				essential::initialize_feed_forward(_neurons, _random_engine);
			}
			break;
		}	// END FEED_FORWARD

		case connection_param::FULL_FORWARD:
		{
			if(_brain_parameters.has_bias_neuron)
			{
				essential::initialize_full_forward_w_bias(_neurons, _random_engine, _bias);
			}
			else
			{
				essential::initialize_full_forward(_neurons, _random_engine);
			}
			break;
		}	// END FULL_FORWARD

		default:
		{
		}
	}	// END switch
}

void network::_reset_netoutputs()
{
	for (auto& layer: _neurons)
	{
		for (auto& neuron: layer)
		{
			neuron.out = 0;
		}
	}
}

void network::_calculate_netoutputs(const std::vector<double>& input)
{
	unsigned input_size = input.size();
	if (input_size != _brain_parameters.structure.neurons_per_layer.front() )
	{
		//Throw an error
	}

	for (unsigned i = 0; i < input_size; ++i)
	{
		_neurons.front()[i].out = input[i];
	}

	if (_brain_parameters.has_bias_neuron)
	{
		_bias.out = 1;
			// The Bias neuron is an additional neuron of the input layer, and always sends a constant signal
	}
	else
	{
		_bias.out = 0;
	}

	for (std::vector<std::vector<neuron>>::iterator layer = ++_neurons.begin(); layer != _neurons.end(); ++layer)
	{
		for (auto& neuron: *layer)
		{
			neuron.activate();
		}
	}
}

void network::_train_network(const std::vector<double>& errors, double learning_rate, double layer_fact)
{
	// Train the output neurons
	for (unsigned i = 0; i < _brain_parameters.structure.neurons_per_layer.back(); ++i) 
	{
		_neurons.back()[i].calculate_delta(errors[i]);
		_neurons.back()[i].train_connection(learning_rate);
	}

	// Train all, but the input/output neurons
	for (std::vector<std::vector<neuron>>::reverse_iterator rlayer = ++_neurons.rbegin();
			rlayer != --_neurons.rend(); ++rlayer)
	{
		learning_rate *= layer_fact;
		for (auto& neuron: *rlayer)
		{
			neuron.calculate_delta();
			neuron.train_connection(learning_rate);
		}
	}
}


std::vector<double> network::_learn_offline(testcases tests, double learning_rate, double layer_fact)
{
	std::vector<std::vector<double>> errors;
	std::vector<double> tmp_error(_brain_parameters.structure.neurons_per_layer.back() );

	for (auto& test: tests)
	{
		if (test[0].size() != _brain_parameters.structure.neurons_per_layer.front() ||
				test[1].size() != _brain_parameters.structure.neurons_per_layer.back() )
		{
			//Throw an error
		}

		_reset_netoutputs();
		_calculate_netoutputs(test.front() );

		for (unsigned j = 0; j < tmp_error.size(); ++j)
		{
			tmp_error[j] = test.back()[j] - _neurons.back()[j].out;
		}
		errors.push_back(tmp_error);
	}

	std::vector<double> avg_error(_brain_parameters.structure.neurons_per_layer.back(), 0);
	for (unsigned i = 0; i < _brain_parameters.structure.neurons_per_layer.back(); ++i)
	{
		for (auto const& error: errors)
		{
			avg_error[i] += error[i];
		}
		avg_error[i] /= (double)errors.size();
	}
	_train_network(avg_error, learning_rate, layer_fact);

	std::vector<double> specific_errors;
	for (auto const& error: errors)
	{
		specific_errors.push_back(_errfct(error) );
	}
	return specific_errors;
}


std::vector<double> network::_learn_online(testcases tests, double learning_rate, double layer_fact)
{
	std::vector<std::vector<double>> errors;
	std::vector<double> tmp_error(_brain_parameters.structure.neurons_per_layer.back() );

	std::shuffle(tests.begin(), tests.end(), _random_engine.get_engine() );
	for (auto& test: tests)
	{
		if (test[0].size() != _brain_parameters.structure.neurons_per_layer.front() ||
				test[1].size() != _brain_parameters.structure.neurons_per_layer.back() )
		{
			//Throw an error
		}

		_reset_netoutputs();
		_calculate_netoutputs(test.front() );

		for (unsigned j = 0; j < tmp_error.size(); ++j)
		{
			tmp_error[j] = test.back()[j] - _neurons.back()[j].out;
		}
		errors.push_back(tmp_error);
		_train_network(errors.back(), learning_rate, layer_fact);
	}

	std::vector<double> specific_errors;
	for (auto const& error: errors)
	{
		specific_errors.push_back(_errfct(error) );
	}
	return specific_errors;
}


double network::train_epoch(testcases tests, double learning_rate, double layer_fact)
{
	std::vector<double> specific_errors;

	switch (_brain_parameters.timing)
	{
		case timing_param::OFFLINE:
		{
			specific_errors = _learn_offline(tests, learning_rate, layer_fact);
			break;
		}	// END OFFLINE

		case timing_param::ONLINE:
		{
			specific_errors = _learn_online(tests, learning_rate, layer_fact);
			break;
		}	// END ONLINE

		default:
		{
		}
	}


	double totalerror = 0;
	for (auto& specific_error: specific_errors)
	{
		totalerror += specific_error;
	}

	return totalerror;
}

std::vector<double> network::solve(const std::vector<double>& input)
{
	_reset_netoutputs();
	_calculate_netoutputs(input);
	
	std::vector<double> solution;
	for (auto const& output_neuron: _neurons.back() )
	{
		solution.push_back(output_neuron.out);
	}
	return solution;
}


void network::monitornet()
{
	/*std::cout << "You have " << _network_structure.size() << " Layers in an" << std::endl
		<< _network_structure.front();
	for (unsigned i = 1; i < _network_structure.size(); ++i)
	{
		std::cout << "-" << _network_structure[i];
	}
	std::cout << " MLP." << std::endl << endl << endl;*/

	std::cout << "Network rates:" << std::endl;
	for (unsigned i = 1; i < _neurons.size(); ++i)
	{
		for (unsigned j = 0; j < _neurons[i].size(); ++j)
		{
			std::cout << "Neuron " << i << "/" << j << ": ";
			_neurons[i][j].monitor();
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}

	std::cout << std::endl << std::endl << "Deltas:" << std::endl;
	for(auto& layer:_neurons)
	{
		for (auto& neuron: layer)
		{
			neuron.monitor_delta();
			std::cout << "  ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}
