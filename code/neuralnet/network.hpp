#pragma once
//------------------------------------------------
//  NETWORK.HPP
//------------------------------------------------

#ifndef NETWORK_HPP
#define NETWORK_HPP


// Libraries
#include <vector>

// My Libraries
#include "neuron.hpp"
#include "rne.hpp"
#include "defs.hpp"

// Typedefs and Structures

// Network : Controls the dynamic of the neural network

class network
{
	private:
		const brain_params _brain_parameters;

		neuron_matrix _neurons; // neuron_matrix from defs.hpp
		neuron _bias;

		rne _random_engine;

		void _initialize_random_rates();

		void _reset_netoutputs();
		void _calculate_netoutputs(const std::vector<double>& input);

		std::vector<double> _learn_offline(testcases tests, double learning_rate, double layer_fact);
		std::vector<double> _learn_online (testcases tests, double learning_rate, double layer_fact);
		void _train_network(const std::vector<double>& errors, double learning_rate, double layer_fact);

	public:
		network(brain_params brain_parameters, rne& random_engine);
		//network(std::string save_file, rne &random_engine);
		
		double train_epoch(testcases tests, double learning_rate = 0.01, double layer_fact = 1.2);
			//testcases from defs.hpp
		std::vector<double> solve(const std::vector<double>& input);
	
		void monitornet();
};

#endif
